import GlobalComponent from './GlobalComponent.js';
import GameBtn from './GameBtn.js';
import GameLevels from './GameLevels.js';
import GameScore from './GameScore.js';


class GameOptions extends GlobalComponent{
    _startBtn = null;
    _levels = null;
    _score = null;

    constructor() {
        super();
        this._startBtn = new GameBtn();
        this._levels = new GameLevels();
        this._score = new GameScore();
        this._createLayout();

    }

    _createLayout() {
        this._element = document.createElement('div');
        this._element.classList.add('game-options');
        this._startBtn.appendTo(this._element);
        this._levels.appendTo(this._element);
        this._score.appendTo(this._element);
    }
}

export default GameOptions;