import GlobalComponent from './GlobalComponent.js';


class GameScore extends GlobalComponent{
    scorePlayer = 0;
    scoreComputer = 0;

    constructor() {
        super();
        this._createLayout();
    }

    incrementPlayer() {
        this.scorePlayer++;
        this._updateLayout();
    }

    incrementComputer() {
        this.scoreComputer++;
        this._updateLayout();
    }

    reset() {
        this.scorePlayer = 0;
        this.scoreComputer = 0;
        this._updateLayout();
    }

    _createLayout() {
        this._element = document.createElement('div');
        this._element.classList.add('score-container');
        this._updateLayout();
    }
    _updateLayout() {
        this._element.innerHTML = `
            <span>Score player: ${this.scorePlayer}</span>
            <span>Score computer: ${this.scoreComputer}</span>
        `;
    }
}

export default GameScore;