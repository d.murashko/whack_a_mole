class GlobalComponent {
    _element = null;

    constructor() {
    }
    appendTo(container) {
        container.append(this._element);
    }
}

export default GlobalComponent;