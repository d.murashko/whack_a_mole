import GlobalComponent from './GlobalComponent.js';

class GameTable extends GlobalComponent{
    cells = [];

    constructor(props) {
        super();
        this.numRows = props.size;
        this.numCellsInRows= props.size;
        this._createLayout();
        this._cursorImageListener(this._element);
    }

    clearTable() {
        this.cells.forEach(item => {
            item.className = 'table-game-cells';
        });
    }

    _createLayout() {
        const tableGame = document.createElement('table');
        tableGame.classList.add('table-game');

        for (let i = 0; i < this.numRows; i++) {
            const tableGameRow = document.createElement('tr');
            for(let j = 0; j < this.numCellsInRows; j++) {
                const tableGameCell = document.createElement('td');
                tableGameCell.classList.add('table-game-cells');
                this.cells.push(tableGameCell);
                tableGameRow.append(tableGameCell);
            }
            tableGame.append(tableGameRow);
        }

        this._element = tableGame;
    }

    _cursorImageListener(elem) {
        elem.addEventListener('mousedown', () => {
            elem.style.cssText = 'cursor: grabbing';
        });

        elem.addEventListener('mouseup', () => {
            elem.removeAttribute("style");
        });
    }

    _setRandomCellActive() {
        let num;
        do {
            num = Math.floor(Math.random() * (this.numRows * this.numCellsInRows));
        } while(this.cells[num].classList.contains('active-cell') ||
        this.cells[num].classList.contains('correct-cell') ||
        this.cells[num].classList.contains('fail-cell'));

        this.cells[num].classList.add('active-cell');
    }
}

export default GameTable;