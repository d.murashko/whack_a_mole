import GlobalComponent from "./GlobalComponent.js";
import GameOptions from "./GameOptions.js";
import GameTable from './GameTable.js';


class Game extends GlobalComponent{
    _gameOptions = null;
    _gameTable = null;

    constructor(props) {
        super();
        this._gameOptions = new GameOptions();
        this._gameTable = new GameTable(props);
        this._createLayout();
        this.appendTo(document.body);
        this.startGame();
    }

    _createLayout() {
        this._element = document.createElement('div');
        this._element.classList.add('game');

        const gameWrap = document.createElement('div');
        gameWrap.classList.add('game-wrapper');

        this._gameOptions.appendTo(gameWrap);
        this._gameTable.appendTo(gameWrap);

        this._element.append(gameWrap);
    }

    startGame() {
        this._gameOptions._startBtn._element.addEventListener('click', (event) => {

            this._gameOptions._startBtn._changeBtnCondition();

            if (this._gameOptions._startBtn.clicked) {
                this._gameOptions._startBtn._changeBtnLayout();
                this._gameOptions._levels._inputs.forEach(item => item.disabled = true);
                event.preventDefault();
                this._gameOptions._levels.chooseLevel();
                this._gameOptions._score.reset();
                this._setCorrectCellClickListener();
                this.intervalFunctionForCells(this._gameOptions._levels._chooseLevel);
            } else {
                clearInterval(this._setIntervalToColorCells);
                this._gameTable.clearTable();
                alert(`You loose! Computer win!!!)))\nIt\`s score: ${this._gameOptions._score.scoreComputer}`);
                this._clearGame();
            }
            // console.log('BTN condition--->', this._gameOptions._startBtn.clicked);
            // console.log('Changed BTN condition--->', this._gameOptions._startBtn.clicked);

            // event.target.classList.add('start-btn--invisible');

        });
    }

    _setCorrectCellClickListener() {
        this._gameTable._element.addEventListener('click', (event) => {
            if(event.target.classList.contains('active-cell')) {
                event.target.classList.remove('active-cell');
                event.target.classList.add('correct-cell');
                this._gameOptions._score.incrementPlayer();
            }
        });
    }

    intervalFunctionForCells(val) {
        this._setIntervalToColorCells = setInterval(this._randomizeCellsToColor.bind(this), val);
    }

    _randomizeCellsToColor() {
        this._setFailCell();
        this._gameTable._setRandomCellActive();
        this._checkWinnerInGame();
    };

    _setFailCell() {
        this._gameTable.cells.forEach(item => {
            if(item.classList.contains('active-cell')) {
                item.classList.remove('active-cell');
                item.classList.add('fail-cell');
                this._gameOptions._score.incrementComputer();
            }
        });
    }

    _checkWinnerInGame() {
        if(this._gameTable.cells.filter(item => item.classList.contains('fail-cell')).length === this._gameTable.cells.length / 2) {
            clearInterval(this._setIntervalToColorCells);
            alert(`You loose! Computer win!!!)))\nIt\`s score: ${this._gameOptions._score.scoreComputer}`);
            this._gameOptions._startBtn._changeBtnCondition();
            this._clearGame();
        } else if(this._gameTable.cells.filter(item => item.classList.contains('correct-cell')).length === this._gameTable.cells.length  / 2) {
            clearInterval(this._setIntervalToColorCells);
            alert(`Congratulation. You Win!\nYour score: ${this._gameOptions._score.scorePlayer}`);
            this._gameOptions._startBtn._changeBtnCondition();
            this._clearGame();
        }
    }

    _clearGame() {
        this._gameTable.clearTable();
        this._gameOptions._levels._inputs.forEach(item => item.disabled = false);
        this._gameOptions._startBtn._changeBtnLayout();
    }
}

export default Game;