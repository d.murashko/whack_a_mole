import GlobalComponent from './GlobalComponent.js';

class GameBtn extends GlobalComponent{

    clicked = false;

    constructor() {
        super();
        this._createLayout();
    }

    _createLayout() {
        const startBtn = document.createElement('button');
        startBtn.innerText = 'Start';
        startBtn.classList.add('btn', 'start-btn');

        this._element = startBtn;
    }

    _changeBtnCondition() {
        this.clicked = !this.clicked;
    }

    _changeBtnLayout() {
        if(this.clicked) {
            this._element.innerText = 'Stop';
            this._element.classList.remove('start-btn');
            this._element.classList.add('stop-btn');
        } else {
            this._element.innerText = 'Start';
            this._element.classList.remove('stop-btn');
            this._element.classList.add('start-btn');
        }

    }
}

export default GameBtn;