import GlobalComponent from './GlobalComponent.js';


class GameLevels extends GlobalComponent{
    _inputs = [];
    _chooseLevel = null;

    constructor() {
        super();
        this._createLayout();
    }
    _createLayout() {
        const levelsContainer = document.createElement('div');
        levelsContainer.classList.add('levels-container');

        const labelBeginer = document.createElement('label');
        const levelBeginer = document.createElement('input');
        levelBeginer.setAttribute('type', 'radio');
        levelBeginer.setAttribute('name', 'level');
        levelBeginer.setAttribute('value', '1500');
        levelBeginer.setAttribute('checked', 'checked');
        this._inputs.push(levelBeginer);
        labelBeginer.innerText = 'Любитель';
        labelBeginer.append(levelBeginer);
        levelsContainer.append(labelBeginer);

        const labelProfi = document.createElement('label');
        const levelProfi = document.createElement('input');
        levelProfi.setAttribute('type', 'radio');
        levelProfi.setAttribute('name', 'level');
        levelProfi.setAttribute('value', '1000');
        this._inputs.push(levelProfi);
        labelProfi.innerText = 'Профи';
        labelProfi.append(levelProfi);
        levelsContainer.append(labelProfi);

        const labelMaster = document.createElement('label');
        const levelMaster = document.createElement('input');
        levelMaster.setAttribute('type', 'radio');
        levelMaster.setAttribute('name', 'level');
        levelMaster.setAttribute('value', '500');
        this._inputs.push(levelMaster);
        labelMaster.innerText = 'Мастер';
        labelMaster.append(levelMaster);
        levelsContainer.append(labelMaster);


        this._element = levelsContainer;
    }

    chooseLevel() {
        this._inputs.forEach(item => {
            if(item.checked) {
                this._chooseLevel = +item.value;
            }
        });
    }
}

export default GameLevels;