**`Whack a mole`**

Once the game starts, the mole will begin to pop up from its holes at random. 
The object of the game is to force  mole back into its holes by hitting its 
directly on the head with the mallet, thereby adding to the player's score. 
The more quickly this is done the higher the final score will be.

**`What was used`**

- HTML
- CSS
- Native JavaScript (on classes)

**`Start game`**

You have to clone this project to your computer and run `index.html`

###### or

click this page: https://d.murashko.gitlab.io/whack_a_mole or https://di-m-whack-a-mole.netlify.app/